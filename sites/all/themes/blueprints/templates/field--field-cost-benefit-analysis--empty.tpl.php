<?php

/**
 * @file field--field-cost-benefit-analysis--empty.tpl.php
 * Template implementation to display the value of a field with description and
 * no field values.
 *
 * Available variables:
 * - $label: The item label.
 * - $label_hidden: Whether the label display is set to 'hidden'.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - field: The current template type, i.e., "theming hook".
 *   - field-name-[field_name]: The current field name. For example, if the
 *     field name is "field_description" it would result in
 *     "field-name-field-description".
 *   - field-type-[field_type]: The current field type. For example, if the
 *     field type is "text" it would result in "field-type-text".
 *   - field-label-[label_display]: The current label position. For example, if
 *     the label position is "above" it would result in "field-label-above".
 * - $description: A string of HTML.
 *
 * Other variables:
 * - $element['#object']: The entity to which the field is attached.
 * - $element['#view_mode']: View mode, e.g. 'full', 'teaser'...
 * - $element['#field_name']: The field name.
 * - $element['#field_type']: The field type.
 * - $element['#field_language']: The field language.
 * - $element['#field_translatable']: Whether the field is translatable or not.
 * - $element['#label_display']: Position of label display, inline, above, or
 *   hidden.
 * - $field_name_css: The css-compatible field name.
 * - $field_type_css: The css-compatible field type.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess_field()
 * @see theme_field()
 *
 * @ingroup themeable
 */
?>
<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php if (!$label_hidden): ?>
    <div class="field-label"<?php print $title_attributes; ?>>
      <?php print $label ?>&nbsp;
    </div>
    <?php if($description): ?>
      <div class="field-description">
        <?php print $description; ?>
      </div>
    <?php endif; ?>
  <?php endif; ?>
  <div class="field-items"<?php print $content_attributes; ?>>
    <div class="field-item even">
      <table class="field-collection-view-final sticky-enabled">
        <thead><tr>
          <th class="field_cba_cost">Cost</th>
          <th class="field_cba_benefits_to_taxpayers"><?php print t('Benefits to Taxpayers'); ?></th>
          <th class="field_cba_benefits_to_particip"><?php print t('Benefits to Participants'); ?></th>
          <th class="field_cba_benefits_to_others"><?php print t('Benefits to Others'); ?></th>
          <th class="field_cba_total_benefits"><?php print t('Total Benefits'); ?></th>
          <th class="field_cba_benefits_minus_costs"><?php print t('Benefits minus costs'); ?></th>
          <th class="field_cba_benefit_cost_ratio"><?php print t('Benefit-Cost Ratio'); ?></th>
          <th class="field_cba_rate_return_investment"><?php print t('Rate of Return on Investment'); ?></th>
          <th class="field_cba_risk_of_loss"><?php print t('Risk of loss'); ?></th>
        </tr></thead>
        <tbody>
        <tr class="field_collection_item odd">
          <td class="field_cba_cost">
            <div class="field field-name-field-cba-cost field-type-number-float field-label-hidden">
              <div class="field-items"><div class="field-item even">&nbsp;</div></div>
            </div>
          </td>
          <td class="field_cba_benefits_to_taxpayers">
            <div class="field field-name-field-cba-benefits-to-taxpayers field-type-number-float field-label-hidden">
              <div class="field-items"><div class="field-item even">&nbsp;</div></div>
            </div>
          </td>
          <td class="field_cba_benefits_to_particip">
            <div class="field field-name-field-cba-benefits-to-particip field-type-number-float field-label-hidden">
              <div class="field-items"><div class="field-item even">&nbsp;</div></div>
            </div>
          </td>
          <td class="field_cba_benefits_to_others">
            <div class="field field-name-field-cba-benefits-to-others field-type-number-float field-label-hidden">
              <div class="field-items"><div class="field-item even">&nbsp;</div></div>
            </div>
          </td>
          <td class="field_cba_total_benefits">
            <div class="field field-name-field-cba-total-benefits field-type-number-float field-label-hidden">
              <div class="field-items"><div class="field-item even">&nbsp;</div></div>
            </div>
          </td>
          <td class="field_cba_benefits_minus_costs">
            <div class="field field-name-field-cba-benefits-minus-costs field-type-number-float field-label-hidden">
              <div class="field-items"><div class="field-item even">&nbsp;</div></div>
            </div>
          </td>
          <td class="field_cba_benefit_cost_ratio">
            <div class="field field-name-field-cba-benefit-cost-ratio field-type-number-decimal field-label-hidden">
              <div class="field-items"><div class="field-item even">&nbsp;</div></div>
            </div>
          </td>
          <td class="field_cba_rate_return_investment">
            <div class="field field-name-field-cba-rate-return-investment field-type-number-float field-label-hidden">
              <div class="field-items"><div class="field-item even">&nbsp;</div></div>
            </div>
          </td>
          <td class="field_cba_risk_of_loss">
            <div class="field field-name-field-cba-risk-of-loss field-type-number-float field-label-hidden">
              <div class="field-items"><div class="field-item even">&nbsp;</div></div>
            </div>
          </td>
        </tr>
        </tbody>
      </table>
    </div>


  </div>
  <div class="field-footer">
    <?php print $footer; ?>
  </div>
</div>