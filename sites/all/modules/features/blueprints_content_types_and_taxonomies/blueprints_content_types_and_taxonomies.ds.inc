<?php
/**
 * @file
 * blueprints_content_types_and_taxonomies.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function blueprints_content_types_and_taxonomies_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|programme|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'programme';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_approved',
        1 => 'field_brief_description',
      ),
      'right' => array(
        2 => 'field_target_group',
        3 => 'field_operating_domain',
        4 => 'field_level_of_intervention',
      ),
      'footer' => array(
        5 => 'field_outcomes_affected',
        6 => 'field_impact_of_programme',
        7 => 'field_impact_interventions_pdf',
        8 => 'field_impact_is_from_report',
        9 => 'group_risk_protective',
        10 => 'field_cost_benefit_analysis',
        11 => 'field_source_of_benefits',
        12 => 'field_comm_risk_sub_term',
        13 => 'field_progr_information_contact',
        14 => 'group_ind_peer_factors',
        15 => 'group_family_factors',
        16 => 'field_local_authorities_uk',
        17 => 'field_comm_protective_sub_term',
        18 => 'group_school_work_factors',
        19 => 'group_community_factors',
        20 => 'group_economic_factors',
        21 => 'field_ind_risk_sub_term',
        22 => 'field_fam_risk_sub_term',
        23 => 'field_ind_protective_sub_term',
        24 => 'field_school_risk_sub_term',
        25 => 'field_fam_protective_sub_term',
        26 => 'field_school_protective_sub_term',
        27 => 'field_econ_risk_sub_term',
        28 => 'field_econ_protective_sub_term',
      ),
    ),
    'fields' => array(
      'field_approved' => 'left',
      'field_brief_description' => 'left',
      'field_target_group' => 'right',
      'field_operating_domain' => 'right',
      'field_level_of_intervention' => 'right',
      'field_outcomes_affected' => 'footer',
      'field_impact_of_programme' => 'footer',
      'field_impact_interventions_pdf' => 'footer',
      'field_impact_is_from_report' => 'footer',
      'group_risk_protective' => 'footer',
      'field_cost_benefit_analysis' => 'footer',
      'field_source_of_benefits' => 'footer',
      'field_comm_risk_sub_term' => 'footer',
      'field_progr_information_contact' => 'footer',
      'group_ind_peer_factors' => 'footer',
      'group_family_factors' => 'footer',
      'field_local_authorities_uk' => 'footer',
      'field_comm_protective_sub_term' => 'footer',
      'group_school_work_factors' => 'footer',
      'group_community_factors' => 'footer',
      'group_economic_factors' => 'footer',
      'field_ind_risk_sub_term' => 'footer',
      'field_fam_risk_sub_term' => 'footer',
      'field_ind_protective_sub_term' => 'footer',
      'field_school_risk_sub_term' => 'footer',
      'field_fam_protective_sub_term' => 'footer',
      'field_school_protective_sub_term' => 'footer',
      'field_econ_risk_sub_term' => 'footer',
      'field_econ_protective_sub_term' => 'footer',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|programme|default'] = $ds_layout;

  return $export;
}
