<?php

/**
 * @file
 * Default theme implementation for displaying search results.
 *
 * This template collects each invocation of theme_search_result(). This and
 * the child template are dependent to one another sharing the markup for
 * definition lists.
 *
 * Note that modules may implement their own search type and theme function
 * completely bypassing this template.
 *
 * Available variables:
 * - $search_results: All results as it is rendered through
 *   search-result.tpl.php
 * - $module: The machine-readable name of the module (tab) being searched, such
 *   as "node" or "user".
 *
 *
 * @see template_preprocess_search_results()
 *
 * @ingroup themeable
 */
?>
<?php if ($search_results): ?>
  <h2 class="title"><span class='iic-icon'>&#57350;</span> <?php print t('Results');?></h2>
  <table class="table search-results <?php print $module; ?>-results">
    <thead>
      <th><?php print t('Intervention');?></th>
      <th><?php print t('Blueprints approved');?></th>
      <th><?php print t('Cost');?></th>
      <th><?php print t('Benefit');?></th>
      <th><?php print t('Benefits minus costs');?></th>
      <th><?php print t('Benefit-cost ratio');?></th>
      <th><?php print t('Rate of return on investment');?></th>
      <th><?php print t('Risk of loss');?></th>
    </thead>
    <tbody>
      <?php print $search_results; ?>
    </tbody>
  </table>
  <?php print $pager; ?>
<?php else : ?>
  <h2><?php print t('Your search yielded no results');?></h2>
  <?php print search_help('search#noresults', drupal_help_arg()); ?>
<?php endif; ?>
