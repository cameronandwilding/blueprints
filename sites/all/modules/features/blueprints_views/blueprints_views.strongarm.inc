<?php
/**
 * @file
 * blueprints_views.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function blueprints_views_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'nodequeue_links';
  $strongarm->value = FALSE;
  $export['nodequeue_links'] = $strongarm;

  return $export;
}
