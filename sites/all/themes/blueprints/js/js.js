/**
 * @file
 * General JS for blueprints.
 */
(function ($) {
  Drupal.behaviors.blueprints = {    
    attach: function (context, settings) {
      // Toggle class on newsletter sign up if input is empty.
      $('#mailchimp-lists-user-subscribe-form-investing-in-children input[type="text"]').blur(function() {
        if ($(this).val() == '') {
          $(this).addClass("is-empty");
        }
        else {
          $(this).removeClass("is-empty");
        }
      });

      //prettyPhoto
      if ($.fn.prettyPhoto) {
        $("a[rel^='prettyPhoto']").prettyPhoto();
      }

      //Portfolio Image Hover
      //$('div.border-img a, .gallery li a').less({ opacity: 0 });
      $(".portfolio li, div.border-img:not(.not-hover)").hover(function () {
        $(this).find("img").stop(true, true).animate({ opacity: 0.1 }, 300);
        $(this).find("a.img-view").animate({left: 50+'%', marginLeft: -52+'px', opacity: 1}, 300, 'easeOutQuart');
        $(this).find("a.img-link").animate({right: 50+'%', marginRight: -52+'px', opacity: 1}, 300, 'easeOutQuart');
      }, function() {
        $(this).find("img").stop(true, true).animate({ opacity: 1 }, 300);
        $(this).find("a.img-view").animate({left: -42+'px', marginLeft: 0, opacity: 0 });
        $(this).find("a.img-link").animate({right:  -42+'px', marginRight: 0, opacity: 0 });
      });

      //FLICKR
      if ($.fn.jflickrfeed) {
        $('#region-sidebar-second .flickr').jflickrfeed({
          limit: 8,
          qstrings: {
            id: '99771506@N00'
          },
          itemTemplate: '<li>'+
            '<a rel="prettyPhoto[flickr]" href="{{image}}" title="{{title}}">' +
            '<img src="{{image_s}}" alt="{{title}}" />' +
            '</a>' +
            '</li>'
        }, function(data) {
          if ($.fn.prettyPhoto) {
            $("a[rel^='prettyPhoto']").prettyPhoto();
          }

          $("#footer .flickr li").hover(function () {
            $(this).find("img").stop(true, true).animate({ opacity: 0.5 }, 800);
          }, function() {
            $(this).find("img").stop(true, true).animate({ opacity: 1.0 }, 800);
          });
        });
      }

      $(window).load(function(){
        var globalCount = 0;
        $('.search-results th').each(function(){
          this.id='tablesort_'+(globalCount++);
        });
      });

      $(".search-results").tablesorter({
        textExtraction: function(node) {
          // For numbers formatted like $1,000.50, €1,000.50 or $1,000.50.
          return $(node).text().replace(/[,$£€]/g,'');
        },
        sortList: [[0,0]],
        headers: {
          1: {sorter: false},
        }
      });

      $(".popoverBlock .block-title").live('mouseover',function(){
        var toolTipFacet = $(this).find(".popover").text();
        $(this).attr('data-tooltip',toolTipFacet);
      });

      var trh1 = $("#trh1").text();
      var trh2 = $("#trh2").text();
      var trh3 = $("#trh3").text();
      var trh4 = $("#trh4").text();
      var trh5 = $("#trh5").text();
      var trh6 = $("#trh6").text();
      var trh7 = $("#trh7").text();


      $(".search-results .header").live('mouseover',function() {
        var hoverID = $(this);
        if (hoverID.attr("id") == "tablesort_2") {
          $(this).attr('data-tooltip',trh2);
        }
        else if (hoverID.attr("id") == "tablesort_3") {
          $(this).attr('data-tooltip',trh3);          
        }
        else if (hoverID.attr("id") == "tablesort_4") {
          $(this).attr('data-tooltip',trh4);
        }
        else if (hoverID.attr("id") == "tablesort_5") {
          $(this).attr('data-tooltip',trh5);
        }
        else if (hoverID.attr("id") == "tablesort_6") {
          $(this).attr('data-tooltip',trh6);
        }
        else if (hoverID.attr("id") == "tablesort_7") {
          $(this).attr('data-tooltip',trh7);
        }

      });

      // #tablesort_1 does not have a header class because the sorter is removed.
      // So we want to handle this differently.
      $(".search-results #tablesort_1").live('mouseover',function() {
        var hoverID = $(this);

        if (hoverID.attr("id") == "tablesort_1") {
          $(this).attr('title',trh1);
          $(this).attr('data-tooltip',trh1);
        }
      });      
      
    }
  };

}(jQuery));