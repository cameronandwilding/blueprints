<?php
/**
 * @file
 * blueprints_contexts.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function blueprints_contexts_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'basic_page';
  $context->description = 'Context for basic page.';
  $context->tag = 'Content types';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'basic_page' => 'basic_page',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'path' => array(
      'values' => array(
        '~interventions' => '~interventions',
        '~cost-benefit' => '~cost-benefit',
        '~standards-evidence' => '~standards-evidence',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-blog_archive-block_1' => array(
          'module' => 'views',
          'delta' => 'blog_archive-block_1',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'blueprints-blueprints_sidebar_content' => array(
          'module' => 'blueprints',
          'delta' => 'blueprints_sidebar_content',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'blueprints-blueprints_twitter' => array(
          'module' => 'blueprints',
          'delta' => 'blueprints_twitter',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content types');
  t('Context for basic page.');
  $export['basic_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'blog_detail';
  $context->description = 'Context for blog content type';
  $context->tag = 'Content types';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'blog_post' => 'blog_post',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'tagclouds-1' => array(
          'module' => 'tagclouds',
          'delta' => '1',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-blog_archive-block' => array(
          'module' => 'views',
          'delta' => 'blog_archive-block',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'blueprints-blueprints_twitter' => array(
          'module' => 'blueprints',
          'delta' => 'blueprints_twitter',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
        'blueprints-blueprints_comment_form_as_block' => array(
          'module' => 'blueprints',
          'delta' => 'blueprints_comment_form_as_block',
          'region' => 'postscript_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content types');
  t('Context for blog content type');
  $export['blog_detail'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'blog_listing';
  $context->description = 'Context for blog listing';
  $context->tag = 'Listing';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'blog' => 'blog',
        'blog/archive/*' => 'blog/archive/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'tagclouds-1' => array(
          'module' => 'tagclouds',
          'delta' => '1',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-blog_archive-block' => array(
          'module' => 'views',
          'delta' => 'blog_archive-block',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'blueprints-blueprints_twitter' => array(
          'module' => 'blueprints',
          'delta' => 'blueprints_twitter',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Context for blog listing');
  t('Listing');
  $export['blog_listing'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'cost_benefit';
  $context->description = 'Explicit to cost benefit page';
  $context->tag = 'Pages';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'cost-benefit' => 'cost-benefit',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'blueprints-blueprints_sidebar_content' => array(
          'module' => 'blueprints',
          'delta' => 'blueprints_sidebar_content',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'blueprints-blueprints_twitter' => array(
          'module' => 'blueprints',
          'delta' => 'blueprints_twitter',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Explicit to cost benefit page');
  t('Pages');
  $export['cost_benefit'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'features_page_menu';
  $context->description = '';
  $context->tag = 'ICC Features';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'features' => 'features',
        'features/*' => 'features/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-blog_archive-block_1' => array(
          'module' => 'views',
          'delta' => 'blog_archive-block_1',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'blueprints-blueprints_sidebar_content' => array(
          'module' => 'blueprints',
          'delta' => 'blueprints_sidebar_content',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'blueprints-blueprints_twitter' => array(
          'module' => 'blueprints',
          'delta' => 'blueprints_twitter',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('ICC Features');
  $export['features_page_menu'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'footer_blocks';
  $context->description = 'Blocks shown on the footer and main content';
  $context->tag = 'Sitewide';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '0',
        ),
        'views-footer_blocks-block' => array(
          'module' => 'views',
          'delta' => 'footer_blocks-block',
          'region' => 'f_first',
          'weight' => '-10',
        ),
        'views-footer_blocks-block_1' => array(
          'module' => 'views',
          'delta' => 'footer_blocks-block_1',
          'region' => 'f_second',
          'weight' => '-10',
        ),
        'mailchimp_lists-investing_in_children' => array(
          'module' => 'mailchimp_lists',
          'delta' => 'investing_in_children',
          'region' => 'f_third',
          'weight' => '-10',
        ),
        'blueprints-contact_information' => array(
          'module' => 'blueprints',
          'delta' => 'contact_information',
          'region' => 'footer_first',
          'weight' => '-10',
        ),
        'blueprints-copyright' => array(
          'module' => 'blueprints',
          'delta' => 'copyright',
          'region' => 'footer_first',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks shown on the footer and main content');
  t('Sitewide');
  $export['footer_blocks'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'home';
  $context->description = 'The home page';
  $context->tag = 'Pages';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-hero_blocks-block' => array(
          'module' => 'views',
          'delta' => 'hero_blocks-block',
          'region' => 'content',
          'weight' => '-10',
        ),
        'views-carousel-block' => array(
          'module' => 'views',
          'delta' => 'carousel-block',
          'region' => 'preface_fourth',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Pages');
  t('The home page');
  $export['home'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'interventions';
  $context->description = 'Explicit to interventions page';
  $context->tag = 'Pages';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'interventions' => 'interventions',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'blueprints-blueprints_sidebar_content' => array(
          'module' => 'blueprints',
          'delta' => 'blueprints_sidebar_content',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'blueprints-blueprints_twitter' => array(
          'module' => 'blueprints',
          'delta' => 'blueprints_twitter',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Explicit to interventions page');
  t('Pages');
  $export['interventions'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'interventions_detail';
  $context->description = 'Context for interventions content type';
  $context->tag = 'Content types';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'programme' => 'programme',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'blueprints-interventions_text' => array(
          'module' => 'blueprints',
          'delta' => 'interventions_text',
          'region' => 'content',
          'weight' => '23',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content types');
  t('Context for interventions content type');
  $export['interventions_detail'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'standards_evidence';
  $context->description = 'Explicit to standards evidence page';
  $context->tag = 'Pages';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'standards-evidence' => 'standards-evidence',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'blueprints-blueprints_sidebar_content' => array(
          'module' => 'blueprints',
          'delta' => 'blueprints_sidebar_content',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'blueprints-blueprints_twitter' => array(
          'module' => 'blueprints',
          'delta' => 'blueprints_twitter',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Explicit to standards evidence page');
  t('Pages');
  $export['standards_evidence'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'taxonomy_listing';
  $context->description = 'Context for taxonomy listing';
  $context->tag = 'Listing';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'taxonomy/term/*' => 'taxonomy/term/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'tagclouds-1' => array(
          'module' => 'tagclouds',
          'delta' => '1',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-blog_archive-block' => array(
          'module' => 'views',
          'delta' => 'blog_archive-block',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'blueprints-blueprints_twitter' => array(
          'module' => 'blueprints',
          'delta' => 'blueprints_twitter',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Context for taxonomy listing');
  t('Listing');
  $export['taxonomy_listing'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'webform';
  $context->description = 'Context for webform content type';
  $context->tag = 'Content types';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'webform' => 'webform',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-webforms-block' => array(
          'module' => 'views',
          'delta' => 'webforms-block',
          'region' => 'content',
          'weight' => '-10',
        ),
        'views-webforms-block_1' => array(
          'module' => 'views',
          'delta' => 'webforms-block_1',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'blueprints-contact_page_block' => array(
          'module' => 'blueprints',
          'delta' => 'contact_page_block',
          'region' => 'postscript_first',
          'weight' => '-10',
        ),
        'webform-client-block-3' => array(
          'module' => 'webform',
          'delta' => 'client-block-3',
          'region' => 'postscript_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content types');
  t('Context for webform content type');
  $export['webform'] = $context;

  return $export;
}
