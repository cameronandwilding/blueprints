(function ($) {

	 $(document).ready(function(){
/*						   
	$(".isotope-element .item-info-overlay").hide();
	$(".isotope-element .views-field-title span a").hide();
	
			$(".views-field-field-portfolio-image").hover(function(){
				$(this).find(".item-info-overlay").fadeTo(250, 1);
				$(this).find(".views-field-title span a").fadeTo(250, 1);
				}, function() {
					$(this).find(".item-info-overlay").fadeTo(250, 0);		
			});
			
	*/
	
	    //prettyPhoto
    $("a[rel^='prettyPhoto']").prettyPhoto();
 
   
    //Portfolio Image Hover
    $('div.border-img a, .gallery li a').css({ opacity: 0 });
    $(".portfolio li, div.border-img:not(.not-hover)").hover(function () {						 
    	$(this).find("img").stop(true, true).animate({ opacity: 0.1 }, 300);
        $(this).find("a.img-view").animate({left: 50+'%', marginLeft: -52+'px', opacity: 1}, 300, 'easeOutQuart');
        $(this).find("a.img-link").animate({right: 50+'%', marginRight: -52+'px', opacity: 1}, 300, 'easeOutQuart');
    }, function() {
    	$(this).find("img").stop(true, true).animate({ opacity: 1 }, 300);
        $(this).find("a.img-view").animate({left: -42+'px', marginLeft: 0, opacity: 0 });
        $(this).find("a.img-link").animate({right:  -42+'px', marginRight: 0, opacity: 0 });
    });
    
    /*
    //Portfolio Image Hover
    $("img.border-img:not(.not-hover)").hover(function () {						 
    	$(this).stop(true, true).animate({ opacity: 0.7 }, 800);
    }, function() {
    	$(this).stop(true, true).animate({ opacity: 1 }, 800);
    });

	/*

	//Portf Image Hover
    $("#isotope-container div.border-img").hover(function () {	
        $(this).find("img").stop(true, true).animate({ opacity: 0.7 }, 300);
    	$(this).find("a.img-view").animate({left: 50+'%', marginLeft: -18+'px', opacity: 1}, 300, 'easeOutQuart');
    }, function() {
    	$(this).find("a.img-view").animate({left: -36+'px', marginLeft: 0, opacity: 0 });
        $(this).find("img").stop(true, true).animate({ opacity: 1 }, 300);
    });
*/

	
		   //FLICKR
    $('#region-sidebar-second .flickr').jflickrfeed({
		limit: 8,
		qstrings: {
			id: '99771506@N00'
		},
		itemTemplate: '<li>'+
						'<a rel="prettyPhoto[flickr]" href="{{image}}" title="{{title}}">' +
							'<img src="{{image_s}}" alt="{{title}}" />' +
						'</a>' +
					  '</li>'
	}, function(data) {
		$("a[rel^='prettyPhoto']").prettyPhoto();

        $("#footer .flickr li").hover(function () {						 
    	   $(this).find("img").stop(true, true).animate({ opacity: 0.5 }, 800);
        }, function() {
    	   $(this).find("img").stop(true, true).animate({ opacity: 1.0 }, 800);
        });
	});

/*	
(function ($) {
Drupal.behaviors.designmd = {
  attach: function (context) {
    $('#search-block-form .form-text', context).autofill({
      value: "Search..."
    });
  } 
};
})(jQuery);
*/




});
}(jQuery));