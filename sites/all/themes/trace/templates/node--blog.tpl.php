 <?php 
if (!$page) { 
?>

 <!-- Blog Post --> 
            <article class="blog-post m-b">

                <!-- Custom -->  
<?php print render($content['field_custom']) ?>

              <!-- Blog Content -->
              <div class="b-t">
                <!-- Blog title -->
                <div class="grid-6 alpha">
<div class="blog-title"><?php print $title; ?></div>
By <span class="blue"><?php print render($name) ?></span><img class="d-i r" src="<?php print base_path() ; ?>sites/all/themes/trace/images/cal.png" alt="cal" /> <?php print format_date($node->created, 'custom', 'M d, Y') ?>
                    <br><br>
<img class="d-i f-l" src="<?php print base_path() ; ?>sites/all/themes/trace/images/tags.png" alt="tags" /><div class="t"> <?php print render($content['field_cattag']) ?> </div>  
                </div>   
                <div class="grid-2 omega">
  <?php if ($field_icon): ?>               
<img class="res" src="<?php print render (file_create_url($node->field_icon['und'][0]['uri'])); ?>" >
<?php endif; ?>	
                </div>              
              </div>
              <div class="clearfix"><!-- clearfix --></div> 
              <!-- body -->
<?php hide($content['comments']); hide($content['links']); print render($content); ?>
              <div class="links-t">
                <!-- Button -->

<a class="b-c" href="<?php print $node_url; ?>">Read more »</a>
                <?php if(module_exists('comment') && ($node->comment == COMMENT_NODE_OPEN)) { ?>
        <div class="comments-count d-i"><span class="comments"><img class="mp" src="<?php print base_path() ; ?>sites/all/themes/trace/images/comm-ico.png" alt="comm" /><span class="comments"><?php print $comment_count; ?></span><a href="<?php print $node_url; ?>">comments</a></div>
        <?php } ;?>
              </div>
              <!-- Horizontal Line -->
              <hr class="m-tb-40">
            </article>
            <!-- Blog Post - End -->
            
             <?php } else { 

$acc = user_load(1);
?>
<!--BEGIN: blog single -->

 <article class="blog-post">


          
                <!-- Blog Image -->  
      <?php print render($content['field_blog_image']) ?>
            <div class="b-t">
             <!-- Blog title -->
                <div class="grid-6 alpha">
<div class="blog-title"><?php print $title; ?></div>
By <span class="blue"><?php print render($name) ?></span><img class="d-i r" src="<?php print base_path() ; ?>sites/all/themes/trace/images/cal.png" alt="cal" /> <?php print format_date($node->created, 'custom', 'M d, Y') ?>
                    <br><br>
<img class="d-i f-l" src="<?php print base_path() ; ?>sites/all/themes/trace/images/tags.png" alt="tags" /><div class="t"> <?php print render($content['field_cattag']) ?> </div>
                </div>   
                <div class="grid-2 omega">
                <?php if ($field_icon): ?>   
<img class="res" src="<?php print render (file_create_url($node->field_icon['und'][0]['uri'])); ?>" >
<?php endif; ?>	
                </div>  
                </div>
              <div class="clearfix"><!-- clearfix --></div>             
                            <!-- Blog Text -->
             <?php hide($content['comments']); hide($content['links']); print render($content); ?>
              
              <!-- Horizontal Line -->
              <hr class="m-tb-40">
            </article>
            <!-- Blog Post - End -->
               <?php print render($content['comments']); ?>      
<?php } ?>

