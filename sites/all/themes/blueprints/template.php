<?php

/**
 * @file
 * This file is empty by default because the base theme chain (Alpha & Omega) provides
 * all the basic functionality. However, in case you wish to customize the output that Drupal
 * generates through Alpha & Omega this file is a good place to do so.
 * 
 * Alpha comes with a neat solution for keeping this file as clean as possible while the code
 * for your subtheme grows. Please read the README.txt in the /preprocess and /process subfolders
 * for more information on this topic.
 */

/**
 * Add tablesort jQuery library.
 */
drupal_add_js(drupal_get_path('theme', 'blueprints') .'/js/tablesort/jquery.tablesorter.js');

/**
 * Implementation of template_preprocess_node().
 */
function blueprints_preprocess_node(&$variables) {
  $node = $variables['node'];
  $variables['date'] = '<span class=\'icon\'>&#xe001;</span>' . format_date($node->created, 'custom', 'j M, Y');
  if (arg(0) == 'taxonomy' && arg(1) == 'term') {
    $variables['content']['links']['node']['#links']['node-readmore']['title'] = t('Read more') . ' &#187;';
    if (!isset($variables['content']['links']['comment']['#links']['comment-comments'])) {
      $variables['content']['links']['comment']['#links']['comment-comments'] = array(
        'title' => t('0 comments'),
        'href' => 'node/' . $variables['node']->nid,
        'attributes' => array(
          'title' => ''
        ),
        'fragment' => 'comments',
        'html' => TRUE
      );
    }
    else {
      $variables['content']['links']['comment']['#links']['comment-comments']['attributes']['title'] = '';
    }
  }
  unset($variables['content']['links']['comment']['#links']['comment-add']);

  //Variables for Interventions content type
  if ($variables['node']->type == 'programme') {
    //Approved
    if ($variables['node']->field_approved[LANGUAGE_NONE][0]['value'] == 1) {
      $variables['content']['field_approved'][0]['#markup'] = '<div id=\'crop\'>&#xe005;</div><h2>' . t('Blueprints Approved') . '</h2>';
    }
    else {
      $variables['content']['field_approved']['#access'] = FALSE;
    }

  }
}

/**
 * Implementation of template_preprocess_region().
 */
function blueprints_preprocess_page(&$variables) {
  //Add selectivizr
  drupal_add_js(drupal_get_path('theme', 'blueprints') . '/js/selectivizr.js');
  if (isset($variables['node']->type) && $variables['node']->type == 'blog_post') {
    $variables['title'] = t('Blog');
  }
}


/**
 * Implements hook_preprocess_block().
 */
function blueprints_preprocess_block(&$variables) {
  if ($variables['block']->module == 'facetapi') {
    // The 'outcomes' block contains custom tokens which will not match the text 'outcomes'.
    // So we have to check if the block title contains the string 'outcomes' in it by using preg_match.
    preg_match('/Outcomes/', $variables['block']->subject, $outcomes_match);

    // Put facet in a grid
    // Unfortunately we have to use the subjects as keys
    if ($outcomes_match[0] == 'Outcomes') {
      // Make this span two fifths
      // (It's called pseudo-two-fifths because actually
      // it's more than two fifths, to account for the column gap
      $variables['classes_array'][] = 'grid-pseudo-two-fifths';
      $variables['classes_array'][] = 'alpha';
    }
    else {
      $variables['classes_array'][] = 'grid-pseudo-one-fifth';
      
      if ($variables['block']->subject == 'Focus of intervention') {
        $variables['classes_array'][] = 'omega';
      }  
    }
  }

  switch ($variables['block']->module) {
    case 'mailchimp_lists':
      $variables['block']->subject = t('Newsletter');
      break;

    case 'tagclouds':
      $variables['block']->subject = t('Tags');
      break;

    default:
      break;
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 *  Add class and placeholders to the newsletter signup form.
 */
function blueprints_form_mailchimp_lists_user_subscribe_form_investing_in_children_alter(&$form, &$form_state, $form_id) {
  // First name field.
  if (isset($form['mailchimp_lists']['mailchimp_investing_in_children']['mergevars']['FNAME'])) {
    $form['mailchimp_lists']['mailchimp_investing_in_children']['mergevars']['FNAME']['#attributes']['placeholder'] = 'First name';
    $form['mailchimp_lists']['mailchimp_investing_in_children']['mergevars']['FNAME']['#attributes']['class'][] = 'is-empty';
    $form['mailchimp_lists']['mailchimp_investing_in_children']['mergevars']['FNAME']['#required'] = TRUE;
  }
  // Last name field.
  if (isset($form['mailchimp_lists']['mailchimp_investing_in_children']['mergevars']['LNAME'])) {
    $form['mailchimp_lists']['mailchimp_investing_in_children']['mergevars']['LNAME']['#attributes']['placeholder'] = 'Last name';
    $form['mailchimp_lists']['mailchimp_investing_in_children']['mergevars']['LNAME']['#attributes']['class'][] = 'is-empty';
    $form['mailchimp_lists']['mailchimp_investing_in_children']['mergevars']['LNAME']['#required'] = TRUE;
  }
  // Email field.
  if (isset($form['mailchimp_lists']['mailchimp_investing_in_children']['mergevars']['EMAIL'])) {
    $form['mailchimp_lists']['mailchimp_investing_in_children']['mergevars']['EMAIL']['#attributes']['placeholder'] = 'Email';
    $form['mailchimp_lists']['mailchimp_investing_in_children']['mergevars']['EMAIL']['#attributes']['class'][] = 'is-empty';
  }
  // Organisation field.
  if (isset($form['mailchimp_lists']['mailchimp_investing_in_children']['mergevars']['ORG'])) {
    $form['mailchimp_lists']['mailchimp_investing_in_children']['mergevars']['ORG']['#attributes']['placeholder'] = 'Organisation';
    $form['mailchimp_lists']['mailchimp_investing_in_children']['mergevars']['ORG']['#attributes']['class'][] = 'is-empty';
    $form['mailchimp_lists']['mailchimp_investing_in_children']['mergevars']['ORG']['#required'] = TRUE;
  }
  // Job title field.
  if (isset($form['mailchimp_lists']['mailchimp_investing_in_children']['mergevars']['JTITLE'])) {
    $form['mailchimp_lists']['mailchimp_investing_in_children']['mergevars']['JTITLE']['#attributes']['placeholder'] = 'Job title';
    $form['mailchimp_lists']['mailchimp_investing_in_children']['mergevars']['JTITLE']['#attributes']['class'][] = 'is-empty';
    $form['mailchimp_lists']['mailchimp_investing_in_children']['mergevars']['JTITLE']['#required'] = TRUE;
  }
}

/**
 * Brief message to display when no results match the query.
 *
 * @see search_help()
 */
function blueprints_apachesolr_search_noresults() {
  $output = "<h2 class='title'><span class='iic-icon'>&#57350;</span> ". t('Results') . '</h2>';
  $output .= '<p>' . t('No results.') . '</p>';
  $output .= '<ul><li>' . t('Check if your spelling is correct, or try removing filters.') .
    '</li><li>' . t('Remove quotes around phrases to match each word individually:') .
    ' <em>' . t('""blue drop"') . ' </em> ' . t('will match less than') . ' <em>' .
    t('blue drop') . '</em></li><li>' . t('You can require or exclude terms using + and -:') .
    ' <em>' . t('big +blue drop') . '</em> ' . t('will require a match on') . ' <em>' .
    t('blue') . '</em> ' .t('while') . ' <em>' . t('big blue -drop') . '</em> ' .
    t('will exclude results that contain') . ' <em>' . t('drop') . '</em>.</li></ul>';
  return $output;
}

/**
 * Implements theme_field().
 *
 * Rewrite the output of fields.
 */
function blueprints_field($variables) {
  $field_name = $variables['element']['#field_name'];
  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<div class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . '&nbsp;</div>';
  }

  // Render the items.
  $output .= '<div class="field-items"' . $variables['content_attributes'] . '>';
  foreach ($variables['items'] as $delta => $item) {
    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
    // Change content based on field.
    switch ($field_name) {
      // Changed boolean field field_impact_is_from_report to custom output.
      case 'field_impact_is_from_report':
        $is_from_report = isset($variables['element']['#items'][$delta]['value']) ? $variables['element']['#items'][$delta]['value'] : 0;
        if ($is_from_report) {
          $content = '<p>Figure(s) taken from this ' . l('report.', 'http://www.wsipp.wa.gov/rptfiles/12-04-1201.pdf') . '</p>';
          $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . $content . '</div>';
        }
        else {
          $content = '<p>Figure(s) taken from Appendix A of this ' . l('report.', 'http://investinginchildren.eu/sites/default/files/Investing%20in%20Children%20-%20Technical%20Report%20%28September%202013%29.pdf') . '</p>';
          $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . $content . '</div>';
        }
        break;

      case 'field_impact_of_programme':
        $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
        $output .= '<p> *' . t('statistically significant at p<0.05') . '</p>';
        break;

      default:
        $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
        break;
    }
  }
  $output .= '</div>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';

  return $output;
}

/**
 * Implements template_preprocess_field().
 *
 *  Add custom markup to field group tables.
 */
function blueprints_preprocess_field(&$variables, $hook) {
  $field_name = $variables['element']['#field_name'];

  switch ($field_name) {

    case 'field_cost_benefit_analysis':
      // Check if cost benefit figures come from a report.
      $is_from_report = isset($variables['element']['#object']->field_cb_is_from_report[LANGUAGE_NONE][0]['value']) ? $variables['element']['#object']->field_cb_is_from_report[LANGUAGE_NONE][0]['value'] : 0;
      $content = '<p>' . t('These figures were converted from USD cited in this') .
        ' ' . l(t('report'), 'http://www.wsipp.wa.gov/rptfiles/12-04-1201.pdf') .
        ' '. t('to GBP using 2011 Purchasing Power Parity conversion rates.') . '</p>';
      // If from report, add link. Otherwise used fallback text.
      $variables['description'] = $is_from_report ? $content : NULL;
      $variables['has_description'] = $is_from_report;
      $variables['footer'] = NULL;
      // Empty fields module doesn't work with field collections that have singular
      // cardinality.
      if (empty($variables['items'])) {
        $variables['theme_hook_suggestions'][] = 'field__field_cost_benefit_analysis__empty';
      }
      else {
        // Template with description printed above table.
        $variables['theme_hook_suggestions'][] = 'field__field_collection_with_description';
      }

      break;

    case 'field_source_of_benefits':
      // Check if cost benefit figures come from a report.
      $is_from_report = isset($variables['element']['#object']->field_sob_is_from_report[LANGUAGE_NONE][0]['value']) ? $variables['element']['#object']->field_sob_is_from_report[LANGUAGE_NONE][0]['value'] : 0;
      $content = '<p>' . t('These figures were converted from USD cited in this') .
        ' ' . l(t('report'), 'http://www.wsipp.wa.gov/rptfiles/12-04-1201.pdf') .
        ' '. t('to GBP using 2011 Purchasing Power Parity conversion rates.') . '</p>';
        // If from report, add link.
      $variables['description'] = $is_from_report ? $content : NULL;
      $variables['has_description'] = $is_from_report;
      // Template with description printed above table.
      $variables['theme_hook_suggestions'][] = 'field__field_collection_with_description';
      // Check if the field collection contains any field with secondary participants.
      $has_seconday_participants = FALSE;
      foreach ($variables['element']['#items'] as $item) {
        $field_collection = entity_load('field_collection_item', array($item['value']));
        $field = field_get_items('field_collection_item', reset($field_collection), 'field_participant');
        $has_seconday_participants = isset($field[0]['value']) && $field[0]['value'] == 1 ? TRUE : $has_seconday_participants;
      }
      $variables['footer'] = $has_seconday_participants ? "* " . t('Secondary participant, otherwise primary') : NULL;

      // This fieldset will always have a value in participant even if other fields
      // are all empty. Hence we can not rely on empty($variables['items']) and
      // have to check this way.
      $emptycells = 0;
      foreach ($variables['items'][0]['#rows'][0]['data'] as $cell) {
        // Count how many "empty" cells there are.
        if (isset($cell['data'][0]['#markup']) && $cell['data'][0]['#markup'] == "&mdash;") {
          $emptycells++;
        }
      }
      // Replace empty markup with blank cell if all shown cells are empty.
      if ($emptycells == 5) {
        foreach ($variables['items'][0]['#rows'][0]['data'] as $key => $cell) {
          $variables['items'][0]['#rows'][0]['data'][$key]['data'][0]['#markup'] = "&nbsp;";
        }
      }
      break;

    case 'field_source_of_benefits_select':
      $is_secondary_participant = isset($variables['element']['#object']->field_participant[LANGUAGE_NONE][0]['value']) ? $variables['element']['#object']->field_participant[LANGUAGE_NONE][0]['value'] : 0;
      if ($is_secondary_participant) {
        $variables['items'][0]['#markup'] .= "*";
      }
      break;

    default:
      break;

  }

  // Array of risk and protective fields. We need to check if all the fields are
  // blank and update their markup if they are. They are all in a field group
  // not a field collection like the other table so we have to update the fields
  // individually.
  $risk_protective_factor_fields = array(
    'field_comm_protective_sub_term',
    'field_comm_risk_sub_term',
    'field_econ_protective_sub_term',
    'field_econ_risk_sub_term',
    'field_fam_protective_sub_term',
    'field_fam_risk_sub_term',
    'field_ind_protective_sub_term',
    'field_ind_risk_sub_term',
    'field_school_protective_sub_term',
    'field_school_risk_sub_term',
  );
  if (in_array($field_name, $risk_protective_factor_fields)) {
    // Cache result so we don't have to look this up on every field.
    $all_fields_are_empty = &drupal_static(__FUNCTION__);
    if (!isset($all_fields_are_empty)) {
      $all_fields_are_empty = _blueprints_check_fields_are_empty($risk_protective_factor_fields, $variables['element']['#object']);
    }
    // If all fields are empty, replace dash with a blank.
    if ($all_fields_are_empty && isset($variables['items'][0])) {
      $variables['items'][0]['#markup'] = "&nbsp;";
    }
  }

}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function blueprints_form_comment_form_alter(&$form, &$form_state, $form_id) {
  // Hide fields.
  $form['author']['homepage']['#access'] = FALSE;
  $form['subject']['#access'] = FALSE;
  // Add placeholder and submit text.
  $form['author']['name']['#attributes']['placeholder'] = t('Name (required)');
  $form['author']['mail']['#attributes']['placeholder'] = t('Email (required)');
  $form['comment_body'][LANGUAGE_NONE][0]['value']['#attributes']['placeholder'] = t('Your comment');
  $form['actions']['submit']['#value'] = t('Add Your Comment');
  // Remove description.
  $form['author']['mail']['#description'] = NULL;
}

/**
 * Implements template_preprocess_comment().
 */
function blueprints_preprocess_comment(&$variables) {
  $image_variables = array(
    'path' => drupal_get_path('theme', 'blueprints') . '/images/user.png',
    'alt' => $variables['author'],
    'attributes' => array(
      'class' => 'comment-user-profile',
    ),
  );
  $variables['picture'] = theme('image', $image_variables);
  $variables['submitted'] = $variables['author'] . ' ' . format_date($variables['elements']['#comment']->changed, 'custom', 'j M, Y') . t(' at ') . format_date($variables['elements']['#comment']->changed, 'custom', 'g:i a');
}

/**
 * Implements hook_js_alter().
 */
function blueprints_js_alter(&$js) {
  unset($js['misc/tableheader.js']);
}

/**
 * Overrides theme_facetapi_link_inactive
 * 
 * @param unknown_type $variables
 */
function blueprints_facetapi_link_inactive($variables) {
  // Remove count from links
  $variables['count'] = NULL;
  return theme_facetapi_link_inactive($variables);
}

/**
 * Implements hook_BASE_FORM_ID_alter().
 *
 * Use base form ID as we don't know if the node ID will be the same in
 * different environments.
 */
function blueprints_form_webform_client_form_alter(&$form, &$form_state, $form_id) {
  // Add placehold text.
  if (isset($form['submitted']['name'])) {
    $form['submitted']['name']['#attributes']['placeholder'] = t('Name (required)');
  }
  if (isset($form['submitted']['email'])) {
    $form['submitted']['email']['#attributes']['placeholder'] = t('Email (required)');
  }
  if (isset($form['submitted']['message'])) {
    $form['submitted']['message']['#attributes']['placeholder'] = t('Message');
  }
}


/**
 * Overrides theme_flexslider_list().
 * 
 * @param unknown_type $variables
 */
function blueprints_flexslider_list(&$variables) {
  // This serves no purpose other than to suppress the
  // php notices that were happening for the homepage flexslider
  foreach($variables['items'] as &$item) {
    if (!isset($item['uri'])) $item['uri'] = NULL;
  }
  return theme_flexslider_list($variables);
}

/**
 * Custom function to see if all fields given for a particular node are empty.
 *
 * @param $fields
 *  An array of fields to check on a node.
 * @param $node
 *  The node object to check.
 *
 * @return BOOLEAN
 *  TRUE if all fields on the node are empty. FALSE if one or more fields has a value.
 */
function _blueprints_check_fields_are_empty($fields, $node) {
  $all_empty = TRUE;
  foreach ($fields as $field_name) {
    $all_empty = !empty($node->$field_name) ? FALSE : $all_empty;
  }
  return $all_empty;
}
