<?php

/**
 * @file
 * Install file for blueprints module
 */

/**
 * Enable the required modules
 */
function blueprints_update_7001(&$sandbox) {
  $modules = array(
    'features',
    'blueprints_content_types_and_taxonomies',
    'blueprints_contexts',
    'blueprints_roles_permissions',
    'blueprints_views',
    'oauth_common',
    'twitter'
  );
  module_enable($modules, TRUE);
}

/**
 * Enable the required modules
 */
function blueprints_update_7002(&$sandbox) {
  $modules = array(
    'blueprints_menus',
    'tagclouds'
  );
  module_enable($modules, TRUE);
}

/**
 * Enable the required modules
 */
function blueprints_update_7003(&$sandbox) {
  $modules = array(
    'link',
    'nodequeue'
  );
  module_enable($modules, TRUE);
}

/**
 * Enable the required modules
 */
function blueprints_update_7004(&$sandbox) {
  $modules = array(
    'uuid',
    'uuid_features'
  );
  module_enable($modules, TRUE);
}

/**
 * Enable the required modules
 */
function blueprints_update_7005(&$sandbox) {
  $modules = array(
    'ds',
    'field_group',
    'field_group_table',
    'backup_migrate',
    'mailchimp'
  );
  module_enable($modules, TRUE);
}

/**
 * Delete fields from intervention that have changed. Enable new modules.
 */
function blueprints_update_7006() {
  // Delete fields that have changed type.
  foreach (array(
    'field_local_auth_programme_uk',
    'field_local_authorities_uk',
    'field_cba_benefit_cost_ratio',
  ) as $field) {
    field_delete_field($field);
  }

  // Enable new modules
  $modules_to_enable = array(
    'field_formatter_settings',
    'empty_fields',
  );
  module_enable($modules_to_enable, TRUE);

  // Revert the feature fields.
  features_revert(array('blueprints_content_types_and_taxonomies' => array('field')));
}

/**
 * Enable mailchimp modules and feature.
 */
function blueprints_update_7007() {
  $modules = array(
    'mailchimp',
    'mailchimp_lists',
    'entity_token',
  );
  module_enable($modules, TRUE);

  // Enable feature.
  features_install_modules(array('blueprints_mailchimp_list'));
}

/**
 * Enable rules module and feature.
 */
function blueprints_update_7008() {
  // Enable module.
  module_enable(array('rules'), TRUE);
  // Enable feature.
  features_install_modules(array('blueprints_rules'));
}

/**
 * Enable omega tools and revert theme settings.
 */
function blueprints_update_7009() {
  // Enable module.
  module_enable(array('omega_tools'), TRUE);
  omega_tools_revert_theme_settings('blueprints');
}

/**
 * Enable multiselect.
 */
function blueprints_update_7010() {
  // Enable module.
  module_enable(array(
    'multiselect',
    'textformatter',
  ), TRUE);
}

/**
 * Enable flexslider modules.
 */
function blueprints_update_7011() {
  module_enable(array(
      'flexslider',
      'flexslider_views',
  ), TRUE);
}

/**
 * Set site front page.
 */
function blueprints_update_7012() {
  variable_set('site_frontpage', 'home');
}

/**
 * Enable blueprints misc features.
 */
function blueprints_update_7013() {
  module_enable(array(
      'blueprints_misc',
  ), TRUE);
}

/**
 * Enable pathauto.
 */
function blueprints_update_7014() {
  module_enable(array(
      'pathauto',
  ), TRUE);
}

/**
 * Enable wysiwyg media embed.
 */
function blueprints_update_7015() {
  module_enable(array(
      'wysiwyg_mediaembed',
  ), TRUE);
}

/**
 * Enable facetapi taxonomy sort.
 */
function blueprints_update_7016() {
  module_enable(array(
      'facetapi_taxonomy_sort',
  ), TRUE);
}

/**
 * Enable google analytics and set tracking ID.
 */
function blueprints_update_7017() {
  // Enable module.
  module_enable(array('googleanalytics'), TRUE);
  // Set variable.
  variable_set('googleanalytics_account', 'UA-9293617-3');
}

/**
 * Set page title variable.
 */
function blueprints_update_7018() {
  // Set variable.
  variable_set('site_name', 'Investing in Children - Social Research Unit at Dartington');
}

/**
 * Disable blueprints menu feature. This is content not configuration.
 */
function blueprints_update_7019() {
  module_disable(array('blueprints_menus'), FALSE);
}

/**
 * Enable EU cookie compliance module.
 */
function blueprints_update_7020() {
  module_enable(array('eu_cookie_compliance'), TRUE);
}

/**
 * Enable twitter.
 */
function blueprints_update_7021() {
  module_enable(array('twitter'), TRUE);
}

/**
 * Enable revisioning module.
 */
function blueprints_update_7022() {
  module_enable(array('revisioning'), TRUE);
}

/**
 * Enable IMCE module.
 */
function blueprints_update_7023() {
  module_enable(array(
    'imce',
    'imce_wysiwyg',
  ), TRUE);
}

/**
 * Enable block class style module.
 */
function blueprints_update_7024() {
  module_enable(array(
  'block_class_styles',
  'blueprints_search',
  'token_insert',
  'token_insert_wysiwyg',
  'token_custom',
  ), TRUE);
}

/**
 * One time update of "Features" node (node-157) url alias from "features" to "feature-157".
 */
function blueprints_update_7025() {
  db_update('url_alias')
    ->fields(array(
      'alias' => 'feature-157',
    ))
    ->condition('source', 'node/157', '=')
    ->condition('alias', 'features', '=')
    ->execute();
}

/**
 * Enable blueprint_context module.
 */
function blueprints_update_7026() {
  module_enable(array(
  'blueprints_context',
  ), TRUE);
}
