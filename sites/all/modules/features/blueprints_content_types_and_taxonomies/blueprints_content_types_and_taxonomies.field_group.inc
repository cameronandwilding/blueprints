<?php
/**
 * @file
 * blueprints_content_types_and_taxonomies.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function blueprints_content_types_and_taxonomies_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_community_factors|node|programme|default';
  $field_group->group_name = 'group_community_factors';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'programme';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_risk_protective';
  $field_group->data = array(
    'label' => 'Community',
    'weight' => '17',
    'children' => array(
      0 => 'field_comm_protective_sub_term',
      1 => 'field_comm_risk_sub_term',
    ),
    'format_type' => 'table',
    'format_settings' => array(
      'label' => 'Community',
      'instance_settings' => array(
        'label_visibility' => '1',
        'desc' => '',
        'first_column' => '',
        'second_column' => '',
        'empty_label_behavior' => '1',
        'table_row_striping' => 0,
        'always_show_field_label' => 0,
        'classes' => ' group-community-factors field-group-table',
      ),
    ),
  );
  $export['group_community_factors|node|programme|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_economic_factors|node|programme|default';
  $field_group->group_name = 'group_economic_factors';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'programme';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_risk_protective';
  $field_group->data = array(
    'label' => 'Economic',
    'weight' => '18',
    'children' => array(
      0 => 'field_econ_protective_sub_term',
      1 => 'field_econ_risk_sub_term',
    ),
    'format_type' => 'table',
    'format_settings' => array(
      'label' => 'Economic',
      'instance_settings' => array(
        'label_visibility' => '1',
        'desc' => '',
        'first_column' => '',
        'second_column' => '',
        'empty_label_behavior' => '1',
        'table_row_striping' => 0,
        'always_show_field_label' => 0,
        'classes' => ' group-economic-factors field-group-table',
      ),
    ),
  );
  $export['group_economic_factors|node|programme|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_family_factors|node|programme|default';
  $field_group->group_name = 'group_family_factors';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'programme';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_risk_protective';
  $field_group->data = array(
    'label' => 'Family',
    'weight' => '15',
    'children' => array(
      0 => 'field_fam_protective_sub_term',
      1 => 'field_fam_risk_sub_term',
    ),
    'format_type' => 'table',
    'format_settings' => array(
      'label' => 'Family',
      'instance_settings' => array(
        'label_visibility' => '1',
        'desc' => '',
        'first_column' => '',
        'second_column' => '',
        'empty_label_behavior' => '1',
        'table_row_striping' => 0,
        'always_show_field_label' => 0,
        'classes' => ' group-family-factors field-group-table',
      ),
    ),
  );
  $export['group_family_factors|node|programme|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ind_peer_factors|node|programme|default';
  $field_group->group_name = 'group_ind_peer_factors';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'programme';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_risk_protective';
  $field_group->data = array(
    'label' => 'Individual and peer',
    'weight' => '14',
    'children' => array(
      0 => 'field_ind_protective_sub_term',
      1 => 'field_ind_risk_sub_term',
    ),
    'format_type' => 'table',
    'format_settings' => array(
      'label' => 'Individual and peer',
      'instance_settings' => array(
        'label_visibility' => '1',
        'desc' => '',
        'first_column' => '',
        'second_column' => '',
        'empty_label_behavior' => '1',
        'table_row_striping' => 0,
        'always_show_field_label' => 0,
        'classes' => ' group-ind-peer-factors field-group-table',
      ),
    ),
  );
  $export['group_ind_peer_factors|node|programme|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_risk_community|node|programme|form';
  $field_group->group_name = 'group_risk_community';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'programme';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_risk_pro';
  $field_group->data = array(
    'label' => 'Community',
    'weight' => '14',
    'children' => array(
      0 => 'field_comm_protective_sub_term',
      1 => 'field_comm_risk_sub_term',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Community',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => ' group-risk-community field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $export['group_risk_community|node|programme|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_risk_economic_factors|node|programme|form';
  $field_group->group_name = 'group_risk_economic_factors';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'programme';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_risk_pro';
  $field_group->data = array(
    'label' => 'Economic',
    'weight' => '15',
    'children' => array(
      0 => 'field_econ_protective_sub_term',
      1 => 'field_econ_risk_sub_term',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Economic',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => ' group-risk-economic-factors field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $export['group_risk_economic_factors|node|programme|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_risk_family|node|programme|form';
  $field_group->group_name = 'group_risk_family';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'programme';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_risk_pro';
  $field_group->data = array(
    'label' => 'Family',
    'weight' => '12',
    'children' => array(
      0 => 'field_fam_protective_sub_term',
      1 => 'field_fam_risk_sub_term',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Family',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => ' group-risk-family field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $export['group_risk_family|node|programme|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_risk_individual|node|programme|form';
  $field_group->group_name = 'group_risk_individual';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'programme';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_risk_pro';
  $field_group->data = array(
    'label' => 'Individual & peer',
    'weight' => '11',
    'children' => array(
      0 => 'field_ind_protective_sub_term',
      1 => 'field_ind_risk_sub_term',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Individual & peer',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => ' group-risk-individual field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $export['group_risk_individual|node|programme|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_risk_protective|node|programme|default';
  $field_group->group_name = 'group_risk_protective';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'programme';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Risk & protective factors targeted',
    'weight' => '9',
    'children' => array(
      0 => 'group_community_factors',
      1 => 'group_economic_factors',
      2 => 'group_family_factors',
      3 => 'group_ind_peer_factors',
      4 => 'group_school_work_factors',
    ),
    'format_type' => 'table',
    'format_settings' => array(
      'label' => 'Risk & protective factors targeted',
      'instance_settings' => array(
        'label_visibility' => '2',
        'desc' => '',
        'first_column' => 'Risk factors',
        'second_column' => 'Protective factors',
        'empty_label_behavior' => '1',
        'table_row_striping' => 0,
        'always_show_field_label' => 0,
        'classes' => ' group-risk-protective field-group-table',
      ),
    ),
  );
  $export['group_risk_protective|node|programme|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_risk_pro|node|programme|form';
  $field_group->group_name = 'group_risk_pro';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'programme';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Risk and Protective Factors Targeted',
    'weight' => '11',
    'children' => array(
      0 => 'group_risk_community',
      1 => 'group_risk_economic_factors',
      2 => 'group_risk_family',
      3 => 'group_risk_individual',
      4 => 'group_risk_school',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Risk and Protective Factors Targeted',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => ' group-risk-pro field-group-div',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_risk_pro|node|programme|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_risk_school|node|programme|form';
  $field_group->group_name = 'group_risk_school';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'programme';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_risk_pro';
  $field_group->data = array(
    'label' => 'School & work',
    'weight' => '13',
    'children' => array(
      0 => 'field_school_protective_sub_term',
      1 => 'field_school_risk_sub_term',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'School & work',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => ' group-risk-school field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $export['group_risk_school|node|programme|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_school_work_factors|node|programme|default';
  $field_group->group_name = 'group_school_work_factors';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'programme';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_risk_protective';
  $field_group->data = array(
    'label' => 'School & work',
    'weight' => '16',
    'children' => array(
      0 => 'field_school_protective_sub_term',
      1 => 'field_school_risk_sub_term',
    ),
    'format_type' => 'table',
    'format_settings' => array(
      'label' => 'School & work',
      'instance_settings' => array(
        'label_visibility' => '1',
        'desc' => '',
        'first_column' => '',
        'second_column' => '',
        'empty_label_behavior' => '1',
        'table_row_striping' => 0,
        'always_show_field_label' => 0,
        'classes' => ' group-school-work-factors field-group-table',
      ),
    ),
  );
  $export['group_school_work_factors|node|programme|default'] = $field_group;

  return $export;
}
