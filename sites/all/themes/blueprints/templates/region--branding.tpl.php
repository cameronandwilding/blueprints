<div<?php print $attributes; ?>>
  <div<?php print $content_attributes; ?>>
    <?php if ($linked_logo_img || $site_name || $site_slogan): ?>
    <div class="branding-data clearfix">
      <?php if ($linked_logo_img): ?>
      <div class="logo-img">
        <?php print $linked_logo_img; ?>
      </div>
      <?php endif; ?>

      <div id='main_menu'>
        <?php print theme('links', array('links' => menu_navigation_links('main-menu'), 'attributes' => array('class'=> array('links', 'main-menu')) ));?>
      </div>

      <div id='magnifying_glass'><a href='<?php print $search_url; ?>' title='Search interventions'/>&#xe000;</a></div>

    </div>
    <?php endif; ?>
    <?php print $content; ?>
  </div>
</div>