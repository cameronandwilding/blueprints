<?php
/**
 * @file
 * blueprints_mailchimp_list.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function blueprints_mailchimp_list_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_default_mailchimp_list().
 */
function blueprints_mailchimp_list_default_mailchimp_list() {
  $items = array();
  $items['investing_in_children'] = entity_import('mailchimp_list', '{
    "name" : "investing_in_children",
    "mc_list_id" : "658cd3ad2e",
    "label" : "Investing in children",
    "description" : "Investing in children mailchimp list.",
    "settings" : {
      "roles" : { "2" : 2, "3" : 3, "4" : 4, "1" : 1 },
      "mergefields" : {
        "FNAME" : "",
        "LNAME" : "",
        "EMAIL" : "mail",
        "ORG" : "",
        "JTITLE" : ""
      },
      "mergefields_display" : { "FNAME" : 1, "LNAME" : 1, "EMAIL" : true, "ORG" : 1, "JTITLE" : 1 },
      "doublein" : 0,
      "show_register_form" : 0,
      "default_register_form_optin" : 0,
      "show_account_form" : 0,
      "form_label" : "",
      "submit_label" : "Subscribe",
      "include_interest_groups" : 0,
      "opt_in_interest_groups" : 0,
      "interest_groups_label" : "Interest Groups",
      "cron" : 0,
      "webhooks" : 0,
      "allow_anonymous" : 1,
      "required" : 0
    }
  }');
  return $items;
}
