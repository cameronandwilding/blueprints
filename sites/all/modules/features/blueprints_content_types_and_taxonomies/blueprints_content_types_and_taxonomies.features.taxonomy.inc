<?php
/**
 * @file
 * blueprints_content_types_and_taxonomies.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function blueprints_content_types_and_taxonomies_taxonomy_default_vocabularies() {
  return array(
    'developmental_outcomes_targeted' => array(
      'name' => 'Outcomes affected',
      'machine_name' => 'developmental_outcomes_targeted',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'level_of_intervention' => array(
      'name' => 'Level of Intervention',
      'machine_name' => 'level_of_intervention',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => -6,
    ),
    'local_authorities_in_the_uk' => array(
      'name' => 'Local authorities in the UK',
      'machine_name' => 'local_authorities_in_the_uk',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'operating_domain' => array(
      'name' => 'Focus of intervention',
      'machine_name' => 'operating_domain',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => -8,
    ),
    'target_group_age' => array(
      'name' => 'Target group age',
      'machine_name' => 'target_group_age',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => -9,
    ),
  );
}
