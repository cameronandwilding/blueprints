<?php
/**
 * @file
 * blueprints_content_types_and_taxonomies.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function blueprints_content_types_and_taxonomies_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Doing well in school',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 7,
    'uuid' => '1374a96f-a7e4-4a79-9bfd-a8fb3e1c5997',
    'vocabulary_machine_name' => 'developmental_outcomes_targeted',
  );
  $terms[] = array(
    'name' => 'School ready',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 6,
    'uuid' => '1741618f-0ff8-41df-b58f-8f2beac77d38',
    'vocabulary_machine_name' => 'developmental_outcomes_targeted',
  );
  $terms[] = array(
    'name' => 'Regulating emotions',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 13,
    'uuid' => '1d4f036d-8f07-47ac-817b-91f23b75ac4e',
    'vocabulary_machine_name' => 'developmental_outcomes_targeted',
  );
  $terms[] = array(
    'name' => 'Family',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 1,
    'uuid' => '2251ed4b-89d6-4b57-95c4-769d22d2918c',
    'vocabulary_machine_name' => 'operating_domain',
  );
  $terms[] = array(
    'name' => 'Healthy birth',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 1,
    'uuid' => '2420c7d8-7a37-42c2-9b1f-953ac29889ad',
    'vocabulary_machine_name' => 'developmental_outcomes_targeted',
  );
  $terms[] = array(
    'name' => '0-2',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '2bb92ab4-013f-4ddb-8a8c-bea176e18110',
    'vocabulary_machine_name' => 'target_group_age',
  );
  $terms[] = array(
    'name' => 'Good relations with peers',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 18,
    'uuid' => '351fdd41-de31-4672-affd-06a2151b79cc',
    'vocabulary_machine_name' => 'developmental_outcomes_targeted',
  );
  $terms[] = array(
    'name' => 'Selective prevention',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 2,
    'uuid' => '35930d08-8924-484d-8504-7115abebf1ee',
    'vocabulary_machine_name' => 'level_of_intervention',
  );
  $terms[] = array(
    'name' => '3-5',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 1,
    'uuid' => '38c42533-2261-45e1-bc36-8fec2addb4ff',
    'vocabulary_machine_name' => 'target_group_age',
  );
  $terms[] = array(
    'name' => 'Individual and peer',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '408b5450-c9f7-4262-acb1-c205c64193c6',
    'vocabulary_machine_name' => 'operating_domain',
  );
  $terms[] = array(
    'name' => 'Indicated prevention',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 3,
    'uuid' => '4edb2bb3-982e-4677-9307-9084e30e1f88',
    'vocabulary_machine_name' => 'level_of_intervention',
  );
  $terms[] = array(
    'name' => 'No suicidal ideation',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 15,
    'uuid' => '541d6e0f-8d09-47c1-b719-3d4be66d657b',
    'vocabulary_machine_name' => 'developmental_outcomes_targeted',
  );
  $terms[] = array(
    'name' => 'Not using substances',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 10,
    'uuid' => '55a572ca-2e48-420d-bb5d-101557078266',
    'vocabulary_machine_name' => 'developmental_outcomes_targeted',
  );
  $terms[] = array(
    'name' => 'Maintenance',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 5,
    'uuid' => '591e4ca3-8653-4799-894d-dd979af30509',
    'vocabulary_machine_name' => 'level_of_intervention',
  );
  $terms[] = array(
    'name' => 'Talking and reading',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 5,
    'uuid' => '5b19a620-82c4-4986-83ff-410a2c15f0c6',
    'vocabulary_machine_name' => 'developmental_outcomes_targeted',
  );
  $terms[] = array(
    'name' => 'No chronic ill-health',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 2,
    'uuid' => '6132248e-8c33-4f84-897e-bb9e6188207e',
    'vocabulary_machine_name' => 'developmental_outcomes_targeted',
  );
  $terms[] = array(
    'name' => 'Universal prevention',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 1,
    'uuid' => '62f0c761-9734-4431-9016-b32bf3c0e775',
    'vocabulary_machine_name' => 'level_of_intervention',
  );
  $terms[] = array(
    'name' => 'Promotion',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '6642ac27-6b79-48ac-9b37-400f3ad6083a',
    'vocabulary_machine_name' => 'level_of_intervention',
  );
  $terms[] = array(
    'name' => 'School and work',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 2,
    'uuid' => '666b85c3-ce17-4014-9415-aca4cdd62f2a',
    'vocabulary_machine_name' => 'operating_domain',
  );
  $terms[] = array(
    'name' => 'Good relations with parents',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 17,
    'uuid' => '71a5c60d-f47d-4843-9a43-cede4289f2db',
    'vocabulary_machine_name' => 'developmental_outcomes_targeted',
  );
  $terms[] = array(
    'name' => 'Treatment',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 4,
    'uuid' => '795fe923-ee97-4dbf-96c1-df006761ec0d',
    'vocabulary_machine_name' => 'level_of_intervention',
  );
  $terms[] = array(
    'name' => 'Good behaviour',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 9,
    'uuid' => '7ce483e9-d4d2-480a-bc2b-363107cd9997',
    'vocabulary_machine_name' => 'developmental_outcomes_targeted',
  );
  $terms[] = array(
    'name' => 'Community',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 3,
    'uuid' => '811e43a1-40fa-403a-bd9c-f90aca070559',
    'vocabulary_machine_name' => 'operating_domain',
  );
  $terms[] = array(
    'name' => '15-18',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 4,
    'uuid' => '874a9c6b-f978-4765-8f76-7d029a8b8364',
    'vocabulary_machine_name' => 'target_group_age',
  );
  $terms[] = array(
    'name' => '6-11',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 2,
    'uuid' => '8e919a39-4423-4794-8fef-9ffb6e8ef234',
    'vocabulary_machine_name' => 'target_group_age',
  );
  $terms[] = array(
    'name' => 'No risky sex',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 11,
    'uuid' => 'a2168867-6662-4947-9cd6-6f0fef3bf88e',
    'vocabulary_machine_name' => 'developmental_outcomes_targeted',
  );
  $terms[] = array(
    'name' => '12-14',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 3,
    'uuid' => 'c4384503-d90c-4e50-a637-c57ae66dc69c',
    'vocabulary_machine_name' => 'target_group_age',
  );
  $terms[] = array(
    'name' => 'Not depressed',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 14,
    'uuid' => 'c695d2a9-a9b3-45d6-b278-8a931477a694',
    'vocabulary_machine_name' => 'developmental_outcomes_targeted',
  );
  $terms[] = array(
    'name' => 'Economic',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 4,
    'uuid' => 'e5e0d53a-c046-4357-8211-85891012a055',
    'vocabulary_machine_name' => 'operating_domain',
  );
  $terms[] = array(
    'name' => 'Not abused or neglected',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 19,
    'uuid' => 'e64600bd-de24-41c4-8ed6-4e9543792aca',
    'vocabulary_machine_name' => 'developmental_outcomes_targeted',
  );
  $terms[] = array(
    'name' => '19-22',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 5,
    'uuid' => 'ed4b4f38-9bd7-481a-84b4-22fef87749ac',
    'vocabulary_machine_name' => 'target_group_age',
  );
  $terms[] = array(
    'name' => 'Healthy weight',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 3,
    'uuid' => 'fbe5ca31-4182-46eb-993f-cf518fbed8b2',
    'vocabulary_machine_name' => 'developmental_outcomes_targeted',
  );
  return $terms;
}
