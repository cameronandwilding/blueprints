(function($){
  // Drupal attachement.
  Drupal.behaviors.Blueprints ={
    attach: function (context, settings) {

      // Create select list from main menu for mobile.
      // Create the dropdown base.
      $("<select />").once().appendTo("#main_menu");

      // Create default option "Go to...".
      $("<option />", {
        "selected": "selected",
        "value"   : "",
        "text"    : "Main menu"
      }).appendTo("#main_menu select");

      // Populate dropdown with menu items.
      $("#main_menu a").once().each(function() {
        var menuLink = $(this);
        $("<option />", {
          "value"   : menuLink.attr("href"),
          "text"    : menuLink.text()
        }).appendTo("#main_menu select");
      });

      // Change location on select change.
      $("#main_menu select").change(function() {
        window.location = $(this).find("option:selected").val();
      });

      // Placeholder fallback for every element that has a placeholder attribute.
      $('[placeholder]').each(function() {
        var $this = $(this);
        var placeholderValue = $this.attr('placeholder');

        if ($this.val() == '') {
          $this.val(placeholderValue);
        }
        // Add/remove placeholder on focus/blur.
        $this.focus(function() {
          if ($this.val() == placeholderValue) {
            $this.val('');
          }
        }).blur(function() {
            // If the user didn't enter any text, show the placeholder text again.
            if ($this.val() == '' || $this.val() == placeholderValue) {
              $this.val(placeholderValue);
            }
          });
        // If the user submits the form, remove the placeholder if it is still there.
        $this.closest('form').submit(function() {
          if ($this.val() == $this.attr('placeholder')) {
            $this.val('');
          }
        });
      });

      // Add active class to blog archive list.
      $('#block-views-blog-archive-block a.active').once().parent().addClass("active-parent");
    }
  };
})(jQuery);