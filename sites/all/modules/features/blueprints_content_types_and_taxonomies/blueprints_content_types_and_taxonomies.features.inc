<?php
/**
 * @file
 * blueprints_content_types_and_taxonomies.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function blueprints_content_types_and_taxonomies_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function blueprints_content_types_and_taxonomies_node_info() {
  $items = array(
    'basic_page' => array(
      'name' => t('Features'),
      'base' => 'node_content',
      'description' => t('Add your IC features in here'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'blog_post' => array(
      'name' => t('Blog post'),
      'base' => 'node_content',
      'description' => t('Articles that will appear in the Blog section'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'hero' => array(
      'name' => t('Hero'),
      'base' => 'node_content',
      'description' => t('A <em>hero</em> is a prominent piece of content, found in the carousel or as a block on the home page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('The title will not be shown on the homepage, and is just for administrative purposes.'),
    ),
    'links' => array(
      'name' => t('Links'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'pages' => array(
      'name' => t('Basic Page'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'programme' => array(
      'name' => t('Intervention'),
      'base' => 'node_content',
      'description' => t('Investing in Children programme'),
      'has_title' => '1',
      'title_label' => t('Programme Name'),
      'help' => '',
    ),
    'webform' => array(
      'name' => t('Webform'),
      'base' => 'node_content',
      'description' => t('Create a new form or questionnaire accessible to users. Submission results and statistics are recorded and accessible to privileged users.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
